package com.najczuk.edu.spring.propertiesdrivenbuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropertiesBuilderApplication {

    public static void main(String[] args) {
        SpringApplication.run(PropertiesBuilderApplication.class, args);
    }

}
