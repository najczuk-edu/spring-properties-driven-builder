package com.najczuk.edu.spring.propertiesdrivenbuilder.component;

public class Foo {
    private String name, surname;

    public Foo(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
