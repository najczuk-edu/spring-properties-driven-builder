package com.najczuk.edu.spring.propertiesdrivenbuilder.component;

public class FooBuilder {
    private String name, surname;

    public static FooBuilder create(){
        return new FooBuilder();
    }

    public FooBuilder name(String name) {
        this.name = name;
        return this;
    }

    public FooBuilder surname(String surname) {
        this.surname = surname;
        return this;
    }

    public Foo build(){
        return new Foo(name,surname);
    }
}
