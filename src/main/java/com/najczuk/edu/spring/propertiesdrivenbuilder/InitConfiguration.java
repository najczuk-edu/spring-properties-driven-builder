package com.najczuk.edu.spring.propertiesdrivenbuilder;

import com.najczuk.edu.spring.propertiesdrivenbuilder.component.Foo;
import com.najczuk.edu.spring.propertiesdrivenbuilder.component.FooProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitConfiguration {
//    This is how I'd like it to work
//    @Bean
//    @ConfigurationProperties("t1.foo")
//    public Foo firstFoo() {
//        return FooBuilder.create().build();
//    }
//
//    @Bean
//    @ConfigurationProperties("t2.foo")
//    public Foo secondFoo() {
//        return FooBuilder.create().build();
//    }

// this works
    @Bean
    @ConfigurationProperties("t1.foo")
    public FooProperties firstFooProperties() {
        return new FooProperties();
    }


    @Bean
    @ConfigurationProperties("t2.foo")
    public FooProperties secondFooProperties() {
        return new FooProperties();
    }

    @Bean
    public Foo firstFoo(FooProperties firstFooProperties) {
        return firstFooProperties.initializeBuilder().build();
    }

    @Bean
    public Foo secondFoo(FooProperties secondFooProperties) {
        return secondFooProperties.initializeBuilder().build();
    }
}
