package com.najczuk.edu.spring.propertiesdrivenbuilder.component;

import org.springframework.stereotype.Component;

@Component
public class FooProperties {
    private String name, surname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public FooBuilder initializeBuilder(){
        return FooBuilder.create().name(getName()).surname(getSurname());
    }
}
