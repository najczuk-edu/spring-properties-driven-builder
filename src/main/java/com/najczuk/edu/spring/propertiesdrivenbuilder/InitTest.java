package com.najczuk.edu.spring.propertiesdrivenbuilder;

import com.najczuk.edu.spring.propertiesdrivenbuilder.component.Foo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InitTest implements CommandLineRunner {
    private Foo foo1,foo2;

    public InitTest(Foo firstFoo, Foo secondFoo) {
        this.foo1 = firstFoo;
        this.foo2 = secondFoo;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println(foo1);
        System.out.println(foo2);
    }

}
